function updatePicture() {
	const picture = document.getElementById("picture");
	picture.src = "/resource/image/" + currentPicture();
	document.getElementById("username").value = currentUsername();
}

function currentUsername() {
	const selector = document.getElementById("user");
	return selector[selector.selectedIndex].label;
}

function currentPicture() {
	const selector = document.getElementById("user");
	return selector[selector.selectedIndex].value;
}
