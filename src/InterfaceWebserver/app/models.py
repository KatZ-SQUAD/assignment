from flask_login import UserMixin
from app import login
import requests

certs = "/home/ksquad/certs/cert.pem"

@login.user_loader
def load_user(id):
    api = "https://localhost:5001/login"
    response = requests.post(api, data={"id": id}, verify=certs)
    response = response.json()

    if response['result'] == "Authenticated":
        user = User(id, response['data']['username'], response['data']['token'], response['data']['admin'])
    else:
        user = None

    return user

class User(UserMixin):
    def __init__(self, id, username, token, admin):
        super().__init__()
        self.id = id
        self.username = username
        self.admin = admin
        self.token = token
