from app import app
from urllib.parse import urlparse
import requests
from flask import render_template, flash, redirect, url_for, send_file, request, jsonify
from flask_login import current_user, login_user, logout_user
from app.models import User
from app.forms import LoginForm, RegistrationForm, SelectForm
from werkzeug.utils import secure_filename

certs = "/home/ksquad/certs/cert.pem"

@app.route("/", methods=["GET", "POST"])
@app.route("/index", methods=["GET", "POST"])
def index():

    # Simplified for Like Form
    #Having the api call before authentication is not a good idea
    #however I wasn't able to authenticate a python request
    #therefore api call is prior to authentication to allow automated testing
    likeResponse = None
    if 'apiUrl' in request.form:
        likeResponse = send_request(request.form['apiUrl'], {key: request.form[key] for key in request.form.keys() if key != 'apiUrl'})
        #This is to make displaying the admin page in an ssrf easier
        #This probably shouldn't be in a real web app
        if type(likeResponse) == dict and "result" in likeResponse:
            likeResponse = likeResponse["result"]

    if not current_user.is_authenticated:
        return redirect(url_for("login"))

    api = "https://localhost:5001/users"
    query = requests.get(api, verify=certs)
    response = query.json()

    selector = SelectForm()
    selector.user.choices = [(response['data']['pictures'][user['username']],user['username']) for user in response['data']['users']]
    # Then autoupdate image using javascript

    api = "https://localhost:5001/like?username=" + current_user.username
    query = requests.get(api, verify=certs)
    response = query.json()

    like = response['data']['like']

    return render_template("index.html", title = "Home", selectForm = selector, user=current_user, like=like, likeResponse = likeResponse)

@app.route("/register", methods=["GET", "POST"])
def register():
    if current_user.is_authenticated:
        return redirect(url_for("index"))
    
    form = RegistrationForm()
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data

        api = "https://localhost:5001/register"
        query = requests.post(api, data={"username": username, "password": password}, verify=certs)
        response = query.json()

        #Checks if username is already taken
        if response['result'] == "Register successful":
            flash("Welcome to CatBook")
            return redirect(url_for("login"))
        else:
            flash(response['data']['message'])
    
    #Renders register page on GET requests and failed registration
    return render_template("register.html", title="Register", form=form)
        

@app.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for("index"))

    form = LoginForm()
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data

        #Authenticates user
        api = "https://localhost:5001/login"
        query = requests.post(api, data={"username": username, "password": password}, verify=certs)
        response = query.json()

        if response['result'] == "Not authenticated":
            flash(response['data']['message'])
        else:
            #Logs in authenticated user
            user = User(response['data']['id'], username, response['data']['token'], response['data']['admin'])
            login_user(user, remember=True)
            return redirect(url_for("index"))

    return render_template("login.html", title="Login", form=form)

@app.route("/logout", methods=["GET", "POST"])
def logout():
    logout_user()
    return redirect(url_for("login"))

@app.route("/upload", methods = ["POST", "GET"])
def upload():
    if current_user.is_authenticated:
        src = None
        if request.method == 'POST':
            if 'file' in request.files:
                file = request.files['file']
                if file.filename != "" and file.filename.split(".")[-1] in app.config["ALLOWED_EXTENSIONS"]:
                # Relay uploaded picture to database server
                    requests.post("https://localhost:5001/upload", files={"file": file}, data={"username":current_user.username, "filename": file.filename}, verify=certs)
                    src = "/resource/image/" + file.filename

        return render_template("upload.html", imgsrc = src)
    else:
        return redirect(url_for("login"))

@app.route("/admin", methods=["GET", "POST"])
def admin():

    #This is intentionally insecure as it is used to better demonstrate SSRF
    #This should come after the authentication check in a real application
    if 'apiUrl' in request.form:
        response = send_request(request.form['apiUrl'], {key: request.form[key] for key in request.form.keys() if key != 'apiUrl'})

    #Check if user is logged in
    #This is to prevent errors when current_user.username and current_user.password are used
    if current_user.is_authenticated and current_user.admin:
        
        # Relay page
        url = "https://localhost:5001/admin"
        page = requests.get(url, verify=certs).content

        return page
    
    #403 error is the default return value
    return render_template("403.html"), 403

# Send request to another API
# This is where SSRF can be exploited usually
def send_request(apiUrl, data):
    url = urlparse(apiUrl)
    api = url.scheme + "://" + url.netloc + url.path
    datalist = url.query.split("&")
    response = requests.post(api, data=data, verify=certs) # Send an API Call to secondary server
    type = response.headers["Content-Type"]
    if type == "application/json":
        return response.json()
    else:
        return response.content

#Gets the image resource from the database server and sends it.
#This is so users can access images on the database server
@app.route('/resource/image/<resource>')
def getImage(resource):
    response = requests.get("https://localhost:5001/resource/image/" + resource, stream = True, verify=certs)
    return send_file(response.raw, mimetype = response.headers["Content-Type"])

#This page is just for testing if the admin page is vulnerable to SSRF
@app.route("/visit", methods=["POST", "GET"])
def visit():
    response = {}
    if "visited" in app.config:
        response["result"] = "True"
    else:
        app.config["visited"] = True
        response["result"] = "False"
    return jsonify(response)
    
