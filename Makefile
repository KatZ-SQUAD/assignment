APP=ksquad.ssrf_assignment

all: build

build:
	docker build --rm --tag=$(APP) .
	docker image prune -f

run:
	docker run --add-host=services.catbook.com:127.0.0.1 -p 0.0.0.0:5000:5000/tcp -it --rm $(APP)

clean:
	docker image rm $(APP)
	docker system prune
