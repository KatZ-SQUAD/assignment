import unittest
import requests
from utils import testing_utilities as util
from certificates import certs

class Test_SSRF(unittest.TestCase):

    def test_index_SSRF(self):
        username = "user"
        password = "pass"
        util.create_user(username, password)
        response = requests.get(util.database + "users", verify=certs).json()
        user_added = False
        for user in response["data"]["users"]:
            if username == user["username"]:
                user_added = True
        #Checks that user was added
        self.assertTrue(user_added)

        cookies = requests.post(util.interface + "login", data = {"username":username, "password":password}, verify=certs).cookies
        for cookie in cookies:
            if cookie.name == "session":
                session = cookie.value

        #Makes post request to index, sets apiUrl to database's delete method
        requests.post(util.interface + "index", data={"apiUrl": util.database + "delete", "username": username}, cookies={"session":session}, verify=certs)
        
        response = requests.get(util.database + "users", verify=certs).json()
        user_deleted = True
        for user in response["data"]["users"]:
            if username == user["username"]:
                user_deleted = False

        #Clean up test user if user wasn't removed by SSRF
        if not user_deleted:
            util.delete_user(username)

        
        self.assertFalse(user_deleted)

    def test_admin_SSRF(self):
        username = "testadmin"
        password = "testpass"
        #Creates test admin
        util.create_user(username, password, admin=True)
        #Login to test admin
        response = requests.post(util.interface + "login", data={"username":username, "password":password}, verify=certs)
        #Gets session cookie to stay logged in
        cookies = response.cookies
        for cookie in cookies:
            if cookie.name == "session":
                session = cookie.value

        #Sends SSRF requests
        requests.post(util.interface + "admin", data={"apiUrl": util.interface + "visit"}, cookies={"session": session}, verify=certs)

        util.delete_user(username)

        response = requests.get(util.interface + "visit", verify=certs).json()
        self.assertFalse(eval(response["result"]))
