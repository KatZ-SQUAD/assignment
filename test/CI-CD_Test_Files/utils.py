import requests
from certificates import certs

class testing_utilities:

    interface = "https://services.catbook.com:5000/"

    database = "https://services.catbook.com:5001/"

    #Creates a user in the database, can make the new user an admin
    def create_user(username, password, admin=False):
        response = requests.post(testing_utilities.database + "register", data={"username": username, "password": password}, verify=certs)
        if admin:
            requests.post(testing_utilities.database + "modify", data={"username": username, "admin": True}, verify=certs)
        return response.json()

    #Deletes a user from the database
    def delete_user(username):
        response = requests.post(testing_utilities.database + "delete", data={"username": username}, verify=certs)
        return response.json()
