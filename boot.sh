#!/bin/bash

# Enable job control
set -m

# Run Both (Database and Interface) Webserver
cd /home/ksquad/DatabaseWebserver/
export FLASK_APP=DatabaseWebserver.py
flask run --port=5001 --host=0.0.0.0 --cert=/home/ksquad/certs/cert.pem --key=/home/ksquad/certs/key.pem &

cd /home/ksquad/InterfaceWebserver/
export FLASK_APP=InterfaceWebserver.py
flask run --port=5000 --host=0.0.0.0 --cert=/home/ksquad/certs/cert.pem --key=/home/ksquad/certs/key.pem &

# boot up both server first
while ! nc -z localhost 5000 && ! nc -z localhost 5001; do   
  sleep 1
done

# CI/CD Test Begin
cd /home/ksquad/test

if ! python3 run_tests.py 
then
	echo
	echo "[INFO] CI/CD failed, stopping run .."
	echo
	exit
else
	echo
	echo "[INFO] CI/CD passed, continue run .."
	echo
	fg %2
fi
