FROM ubuntu:18.04

# Update and install system deps
RUN apt-get update && apt-get install -y python3 python3-pip netcat

RUN useradd -m ksquad

###
# Creates Database Server (Server 1)
###

COPY src/DatabaseWebserver/ /home/ksquad/DatabaseWebserver/
WORKDIR /home/ksquad/DatabaseWebserver/
RUN pip3 install -r requirements.txt
RUN chown -R ksquad:ksquad /home/ksquad/DatabaseWebserver

###
# Creates Interface Server (Server 2)
###

COPY src/InterfaceWebserver/ /home/ksquad/InterfaceWebserver/
WORKDIR /home/ksquad/InterfaceWebserver/
RUN pip3 install -r requirements.txt
RUN chown -R ksquad:ksquad /home/ksquad/InterfaceWebserver

WORKDIR /home/ksquad/

###
# Configure SSL
###
RUN mkdir certs
COPY src/SSL_Certificates /home/ksquad/certs/
RUN chown -R ksquad:ksquad /home/ksquad/certs/

###
# CI/CD Testing Configuration
###
RUN mkdir test
COPY test/CI-CD_Test_Files test/

###
# Run both server
###
COPY boot.sh .
RUN chown ksquad:ksquad /home/ksquad/boot.sh
USER ksquad
RUN chmod +x ./boot.sh

ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
EXPOSE 5000
ENTRYPOINT ["./boot.sh"]
